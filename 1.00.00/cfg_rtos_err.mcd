{
    "cfgFiles": [
        {
            "contents": [
                {
                    "contents": [
                        {
                            "text": "No files including rtos_err.h must be included by this file. This could lead to circular inclusion problems.",
                            "type": "note"
                        }
                    ],
                    "title": "INCLUDE FILES NOTE",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "RTOS_ERR_CFG_LEGACY_EN is provided to allow usage of existing code without needing to modify anything regarding error codes. New applications should either completely remove this configuration define  or set it to DEF_DISABLED, to allow usage of extended errors and other advantages such as unified error codes.",
                            "type": "note"
                        },
                        {
                            "text": "RTOS_ERR_CFG_EXT_EN allows to configure whether the RTOS_ERR type contains only an error code (DEF_DISABLED) or contains more debug information (DEF_ENABLED). If set to DEF_ENABLED, a string containing the file name and line number where the error has been set and also the function name, if compiling in C99, will be included in the RTOS_ERR type. Setting this configuration to DEF_ENABLED may have an impact on performance and resource usage, it is recommended to set it to DEF_DISABLED once development is complete. This feature cannot be used if RTOS_ERR_CFG_LEGACY_EN is set to DEF_ENABLED.",
                            "type": "note"
                        },
                        {
                            "text": "RTOS_ERR_CFG_STR_EN allows to have strings associated with each error code, in order to print them. If set to DEF_DISABLED, the error code enum value will be outputted instead. For example, if set to DEF_ENABLED, it would be possible to print RTOS_ERR_NONE or RTOS_ERR_INVALID_ARG as a string instead of printing the numerical value associated, which would be 0 for RTOS_ERR_NONE and higher than 0 for RTOS_ERR_INVALID_ARG.",
                            "type": "note"
                        },
                        {
                            "cfgName": "",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "RTOS_ERR_CFG_LEGACY_EN",
                            "level": "basic",
                            "shortDesc": "",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Enable extended error type.",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "RTOS_ERR_CFG_EXT_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable extended error that allows to embed more information in the error variable when an error occurs. Information includes the file name and line number where the error has been set. Enabling this feature may have an impact on performance, it is recommended to disable it once development is complete.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Enable error strings.",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "RTOS_ERR_CFG_STR_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable error strings to print error in a string format and to print a description of each error code value. If RTOS_ERR_CFG_EXT_EN also set to DEF_ENABLED, strings will be contained in the RTOS_ERR type.",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "DEFINES",
                    "type": "category"
                }
            ],
            "fileName": "rtos_err_cfg.h",
            "includeFiles": [
                "<rtos/common/include/lib_def.h>"
            ],
            "title": "RTOS_ERR CONFIGURATION"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}