{
    "cfgFiles": [
        {
            "contents": [
                {
                    "contents": [
                        {
                            "text": "TCP-IP code may call optimized assembly functions. Optimized assembly files/functions must be included in the project to be enabled.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Configure network protocol suite's assembly optimization (see Note #1)",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_CFG_OPTIMIZE_ASM_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable assembly optimization",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "NETWORK OPTIMIZATION CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "Configure NET_DBG_CFG_MEM_CLR_EN to enable/disable the network protocol suite from clearing internal data structure memory buffers; a convenient feature while debugging.",
                            "type": "note"
                        },
                        {
                            "text": "Configure NET_CTR_CFG_STAT_EN to enable/disable network protocol suite statistics counters.",
                            "type": "note"
                        },
                        {
                            "text": "Configure NET_CTR_CFG_ERR_EN  to enable/disable network protocol suite error counters.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Configure memory clear feature (see Note #1)",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_DBG_CFG_MEM_CLR_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Data structure clears",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure statistics counter feature (see Note #2)",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_CTR_CFG_STAT_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Stat counters",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure error counter feature (see Note #3)",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_CTR_CFG_ERR_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Error counters",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "NETWORK DEBUG CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "Configure Buffer usage statistic counters",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_STAT_POOL_BUF_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Buffer usage statistic counters",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure ARP cache usage statistic counters",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_STAT_POOL_ARP_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable ARP cache usage statistic counters",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure NDP cache usage statistic counters",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_STAT_POOL_NDP_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable NDP cache usage statistic counters",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure IGMP usage statistic counters",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_STAT_POOL_IGMP_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable IGMP usage statistic counters",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure MLDP usage statistic counters",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_STAT_POOL_MLDP_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable MLDP usage statistic counters",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": " NETWORK STATISTIC POOL CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "Configure maximum number of network interfaces",
                            "defaultValue": "2",
                            "defineName": "NET_IF_CFG_MAX_NBR_IF",
                            "level": "advanced",
                            "shortDesc": "Maximum number of network interfaces",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Configure interface transmit suspend timeout in ms",
                            "defaultValue": "1",
                            "defineName": "NET_IF_CFG_TX_SUSPEND_TIMEOUT_MS",
                            "level": "advanced",
                            "shortDesc": "Configure interface transmit suspend timeout in ms",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Configure Loopback interface",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_IF_CFG_LOOPBACK_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable loopback interface",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure interface Wait for setup feature",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IF_CFG_WAIT_SETUP_READY_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable wait for interface setup feature",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "NETWORK INTERFACE LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "Address resolution protocol ONLY required for IPv4.",
                            "type": "note"
                        },
                        {
                            "cfgName": "ARP cache",
                            "defaultValue": "5",
                            "defineName": "NET_ARP_CFG_CACHE_NBR",
                            "level": "advanced",
                            "shortDesc": "Configure ARP cache size",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "NETWORK INTERFACE LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "Neighbor Discovery Protocol ONLY required for IPv6.",
                            "type": "note"
                        },
                        {
                            "cfgName": "NDP Neighbor cache",
                            "defaultValue": "5",
                            "defineName": "NET_NDP_CFG_CACHE_NBR",
                            "level": "advanced",
                            "shortDesc": "Configures number of NDP Neighbor cache entries",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "NDP Destination cache",
                            "defaultValue": "5",
                            "defineName": "NET_NDP_CFG_DEST_NBR",
                            "level": "advanced",
                            "shortDesc": "Configures number of NDP Destination cache entries",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "NDP Prefix cache",
                            "defaultValue": "5",
                            "defineName": "NET_NDP_CFG_PREFIX_NBR",
                            "level": "advanced",
                            "shortDesc": "Configures number of NDP Prefix cache entries",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "NDP Router cache",
                            "defaultValue": "3",
                            "defineName": "NET_NDP_CFG_ROUTER_NBR",
                            "level": "advanced",
                            "shortDesc": "Configures number of NDP Router cache entries",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "NEIGHBOR DISCOVERY PROTOCOL LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "Configure IPv4",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IPv4_CFG_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable IPv4 support",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Configure link-local",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IPv4_CFG_LINK_LOCAL_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable link-local support",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "IPv4 addresses per interface",
                            "defaultValue": "4",
                            "defineName": "NET_IPv4_CFG_IF_MAX_NBR_ADDR",
                            "level": "advanced",
                            "shortDesc": "Configure maximum number of addresses per interface",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Configure IPv6",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IPv6_CFG_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable IPv6 support",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "IPv6 Stateless Address Auto-Configuration",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IPv6_CFG_ADDR_AUTO_CFG_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable IPv6 Auto-Configuration",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "IPv6 Duplication Address Detection (DAD)",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_IPv6_CFG_DAD_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable IPv6 DAD",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "IPv6 addresses per interface",
                            "defaultValue": "8",
                            "defineName": "NET_IPv6_CFG_IF_MAX_NBR_ADDR",
                            "level": "advanced",
                            "shortDesc": "Configure maximum number of addresses per interface",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "INTERNET PROTOCOL LAYER VERSION CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "IPv4 multicast support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_MCAST_CFG_IPv4_RX_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Multicast reception",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "IPv4 multicast support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_MCAST_CFG_IPv4_TX_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Multicast transmission",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Multicast group",
                            "defaultValue": "8",
                            "defineName": "NET_MCAST_CFG_HOST_GRP_NBR_MAX",
                            "level": "advanced",
                            "shortDesc": "Configure maximum number of Multicast groups",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "INTERNET GROUP MANAGEMENT PROTOCOL(MULTICAST) LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "The maximum accept queue size represents the number of connection that can be queued by the stack before being accepted. For a TCP server when a connection is queued, it means that the SYN, ACK packet has been sent back, so the remote host can start transmitting data once the connection is queued and the stack will queue up all data received until the connection is accepted and the data is read.",
                            "type": "note"
                        },
                        {
                            "contents": [
                                {
                                    "text": "It represents the number of bytes that can be queued by one socket. It's important that all socket are not able to queue more data than what the device can hold in its buffers.",
                                    "type": "note"
                                },
                                {
                                    "text": "The size should be also a multiple of the maximum segment size (MSS) to optimize performance. UDP MSS is 1470 and TCP MSS is 1460.",
                                    "type": "note"
                                },
                                {
                                    "text": "RX and TX queue size can be reduce at runtime using socket option API.",
                                    "type": "note"
                                },
                                {
                                    "text": "Window calculation example:\n    Number of TCP connection  : 2\n    Number of UDP connection  : 0\n    Number of RX large buffer : 10\n    Number of TX Large buffer : 6\n    Number of TX small buffer : 2\n    Size of RX large buffer   : 1518\n    Size of TX large buffer   : 1518\n    Size of TX small buffer   : 60\n\n    TCP MSS RX                = 1460\n    TCP MSS TX large buffer   = 1460\n    TCP MSS TX small buffer   = 0\n\n    Maximum receive  window   = (10 * 1460)           = 14600 bytes\n    Maximum transmit window   = (6  * 1460) + (2 * 0) = 8760  bytes\n\n    RX window size per socket = (14600 / 2)           =  7300 bytes\n    TX window size per socket = (8760  / 2)           =  4380 bytes",
                                    "type": "note"
                                }
                            ],
                            "text": "Receive and transmit queue size MUST be properly configured to optimize performance.",
                            "type": "note"
                        },
                        {
                            "cfgName": "UDP connections",
                            "defaultValue": "2",
                            "defineName": "NET_SOCK_CFG_SOCK_NBR_UDP",
                            "level": "advanced",
                            "shortDesc": "Configure number of UDP connections",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "TCP connections",
                            "defaultValue": "5",
                            "defineName": "NET_SOCK_CFG_SOCK_NBR_TCP",
                            "level": "advanced",
                            "shortDesc": "Configure number of TCP connections",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Socket select functionality",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_SOCK_CFG_SEL_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/Disable Socket select",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "TCP Accept queue",
                            "defaultValue": "2",
                            "defineName": "NET_SOCK_CFG_CONN_ACCEPT_Q_SIZE_MAX",
                            "level": "advanced",
                            "shortDesc": "Configure stream-type sockets' accept queue maximum size. (See Note # 1)",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Sockets' Receive buffer sizes (see Note #2)",
                            "defaultValue": "4096",
                            "defineName": "NET_SOCK_CFG_RX_Q_SIZE_OCTET",
                            "level": "advanced",
                            "shortDesc": "Configure socket receive buffer size in number of octets ",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Sockets' Transmit buffer sizes (see Note #2)",
                            "defaultValue": "4096",
                            "defineName": "NET_SOCK_CFG_TX_Q_SIZE_OCTET",
                            "level": "advanced",
                            "shortDesc": "Configure socket transmit buffer size in number of octets ",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "NETWORK SOCKET LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "TCP support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_TCP_CFG_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable TCP Layer",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "TRANSMISSION CONTROL PROTOCOL LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "Configure NET_UDP_CFG_RX_CHK_SUM_DISCARD_EN to enable/disable discarding of UDP datagrams received with NO computed checksum:\n\n    When ENABLED,  ALL UDP datagrams received without a checksum are discarded.\n    When DISABLED, ALL UDP datagrams received without a checksum are flagged so that\n    application(s) may handle &/or discard.",
                            "type": "note"
                        },
                        {
                            "text": "Configure NET_UDP_CFG_TX_CHK_SUM_EN to enable/disable transmitting UDP datagrams with checksums :\n\n    When ENABLED,  ALL UDP datagrams are transmitted with a computed checksum.\n    When DISABLED, ALL UDP datagrams are transmitted without a computed checksum.",
                            "type": "note"
                        },
                        {
                            "cfgName": "UDP Receive Checksum",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_UDP_CFG_RX_CHK_SUM_DISCARD_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Discard on packet with invalid checksum",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "UDP Transmit Checksum",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "NET_UDP_CFG_TX_CHK_SUM_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable Transmit Checksums",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "USER DATAGRAM PROTOCOL LAYER CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "The network security layer can be enabled ONLY if the application project contains a secure module supported by Micrium OS Net such as:\n    - NanoSSL provided by Mocana.\n    - wolfSSL (formerly CyaSSL) provided by wolfSSL.\n    - mbed TLS.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Server secure sockets",
                            "defaultValue": "2",
                            "defineName": "NET_SECURE_CFG_MAX_NBR_SOCK_SERVER",
                            "level": "advanced",
                            "shortDesc": "Configure number of server sockets",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Client secure sockets",
                            "defaultValue": "2",
                            "defineName": "NET_SECURE_CFG_MAX_NBR_SOCK_CLIENT",
                            "level": "advanced",
                            "shortDesc": "Configure number of client sockets",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Server certificates",
                            "defaultValue": "2048",
                            "defineName": "NET_SECURE_CFG_MAX_CERT_LEN",
                            "level": "advanced",
                            "shortDesc": "Configure servers certificate maximum length (bytes)",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Server keys",
                            "defaultValue": "2048",
                            "defineName": "NET_SECURE_CFG_MAX_KEY_LEN",
                            "level": "advanced",
                            "shortDesc": "Configure servers key maximum length (bytes) ",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "Certificate authorities",
                            "defaultValue": "1",
                            "defineName": "NET_SECURE_CFG_MAX_NBR_CA",
                            "level": "advanced",
                            "shortDesc": "Configure maximum number of authorities",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        },
                        {
                            "cfgName": "CA certificate",
                            "defaultValue": "2048",
                            "defineName": "NET_SECURE_CFG_MAX_CA_CERT_LEN",
                            "level": "advanced",
                            "shortDesc": "Configure CA certificate maximum length (bytes)",
                            "suffix": "u",
                            "type": "configuration",
                            "values": "integer"
                        }
                    ],
                    "title": "NETWORK SECURITY MANAGER CONFIGURATION (SSL/TLS)",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "DHCP client Support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_DHCP_CLIENT_CFG_MODULE_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable DHCP client module",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "DNS client Support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "NET_DNS_CLIENT_CFG_MODULE_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable DNS client module",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "NETWORK MODULES CONFIGURATION",
                    "type": "category"
                }
            ],
            "fileName": "net_cfg.h",
            "includeFiles": [
                "<rtos/cpu/include/cpu.h>"
            ],
            "title": "NETWORK CONFIGURATION"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}