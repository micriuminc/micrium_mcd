{
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "cfgElements": [
                        {
                            "contents": [
                                {
                                    "contents": [
                                        {
                                            "cfgName": "Example File System Init",
                                            "defaultValue": "",
                                            "defineName": "EX_FS_INIT_AVAIL",
                                            "type": "configuration"
                                        }
                                    ],
                                    "title": "EXAMPLE MODULES INIT DESCRIPTION",
                                    "type": "category"
                                }
                            ],
                            "internalName": "ex_description.h"
                        }
                    ],
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_utils.h",
                                "examples/fs/ex_fs.c",
                                "examples/fs/ex_fs.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.fs",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.init",
            "inheritParentVersion": true,
            "longDescription": "This example initializes the File System module (Core and Storage layers), creates a cache instance shared by all media, initializes the Media Polling example if it is needed and manages low-level format of media and high-level format of a unique partition.",
            "multiplicity": "1",
            "name": "File System Initialization Example",
            "order": 48,
            "shortDescription": "Initializes File System module."
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_blk_dev_rd_wr.c",
                                "examples/fs/ex_fs_blk_dev_rd_wr.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.blk_dev_rd_wr",
            "inheritParentVersion": true,
            "longDescription": "This example illustrates the usage of the Block Device API allowing raw accesses to the media. The example performs a series of block writes with a data pattern followed by a series of block read to verify the correct data integrity.\r\n\r\n** IMPORTANT NOTE ** You should run this example with extra care. The Block Device layer is NOT aware of the file system formatting on the media. The example writes in the first media sectors. Thus any file system formatting information contained in some sectors may be corrupted. The directories and files may also be corrupted. You should run this example on a media not formatted. If the media was formatted and the example is executed, you must re-format the media. If the media needs to be re-formatted, please enable the following constants in ex_fs.c:\nEX_CFG_FS_MEDIA_LOW_LEVEL_FMT_EN\nEX_CFG_FS_MEDIA_HIGH_LEVEL_FMT_EN",
            "multiplicity": "1",
            "name": "Block Device Read-Write",
            "order": 50,
            "shortDescription": "Example for FS block device read-write"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_file_rd_wr_posix.c",
                                "examples/fs/ex_fs_file_rd_wr_posix.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.file_rd_wr_posix",
            "inheritParentVersion": true,
            "longDescription": "This example shows a typical usage of file write and read functions (using the Posix API). The example writes some data pattern to a specified file and read these data. After the file read, a data comparison is done to ensure the correct data integrity.",
            "multiplicity": "1",
            "name": "File Read-Write (Posix API)",
            "order": 50,
            "shortDescription": "Example for FS file read-write"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_file_rd_wr.c",
                                "examples/fs/ex_fs_file_rd_wr.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.file_rd_wr",
            "inheritParentVersion": true,
            "longDescription": "This example shows a typical usage of file write and read functions (using the File System's native API). The example writes some data pattern to a specified file and read these data. After the file read, a data comparison is done to ensure the correct data integrity.",
            "multiplicity": "1",
            "name": "File Read-Write (Native API)",
            "order": 49,
            "shortDescription": "Example for FS file read-write"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_file_multi_desc.c",
                                "examples/fs/ex_fs_file_multi_desc.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.file_multi_desc",
            "inheritParentVersion": true,
            "longDescription": "This example shows the usage of opening different file descriptors on the same file. A Writer task opens the file in write mode and a Reader task opens the same file in read-only mode. The Writer task writes N logical blocks to the specified file. The Reader task reads N logical blocks from the same file and verifies data integrity of each block.",
            "multiplicity": "1",
            "name": "File Multi-Descriptor",
            "order": 50,
            "shortDescription": "Example for FS file multi-descriptor"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_entry_path.c",
                                "examples/fs/ex_fs_entry_path.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.entry_path",
            "inheritParentVersion": true,
            "longDescription": "This example shows the usage of absolute and relative path creation and manipulation.",
            "multiplicity": "1",
            "name": "Entry Path",
            "order": 50,
            "shortDescription": "Example for FS entry path"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "cfgElements": [
                        {
                            "contents": [
                                {
                                    "contents": [
                                        {
                                            "cfgName": "Example Media Polling Init",
                                            "defaultValue": "",
                                            "defineName": "EX_FS_MEDIA_POLL_INIT_AVAIL",
                                            "type": "configuration"
                                        }
                                    ],
                                    "title": "EXAMPLE MODULES INIT DESCRIPTION",
                                    "type": "category"
                                }
                            ],
                            "internalName": "ex_description.h"
                        }
                    ],
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "examples/fs/ex_fs_media_poll.c",
                                "examples/fs/ex_fs_media_poll.h",
                                "examples/fs/ex_fs_file_rd_wr.c",
                                "examples/fs/ex_fs_file_rd_wr.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.examples.fs.init",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.fs.driver.nand"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.nor"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.ramdisk"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.scsi"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.card"
                                },
                                {
                                    "id": "com.micrium.rtos.fs.driver.sd.spi"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.examples.fs.media_poll",
            "inheritParentVersion": true,
            "longDescription": "This example shows how the application can be notified about insertion and removal of removable media. It targets mainly SD cards and SCSI devices. The Micrium OS File System supports a media polling feature enabled via the configuration FS_STORAGE_CFG_MEDIA_POLL_TASK_EN in fs_storage_cfg.h. Note that if FS_STORAGE_CFG_MEDIA_POLL_TASK_EN is disabled, the media polling example will still work with SCSI devices. SCSI devices insertion/removal are detected regardless of FS_STORAGE_CFG_MEDIA_POLL_TASK_EN configuration. Upon insertion of a removable media, an application connection callback is called by the File System. The same is done upon removal with an application disconnection callback. Once connected, a simple file read/write operation is performed on the media.\r\nThis example can work also with fixed media such as NAND, NOR and RAM Disk. For fixed media, if FS_STORAGE_CFG_MEDIA_POLL_TASK_EN is enabled, the File System will only call the application connection callback.",
            "multiplicity": "1",
            "name": "Media Polling",
            "order": 50,
            "shortDescription": "Example for FS media polling"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}