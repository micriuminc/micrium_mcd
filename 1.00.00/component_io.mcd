{
    "categories": [
        {
            "id": "com.micrium.rtos.io",
            "name": "I/O Modules",
            "order": 40,
            "shortDescription": "I/O Modules, such as SPI"
        },
        {
            "id": "com.micrium.rtos.io.spi",
            "name": "SPI Bus Module",
            "shortDescription": "SPI Bus Module"
        },
        {
            "id": "com.micrium.rtos.io.serial.drivers",
            "name": "SPI Bus Drivers",
            "shortDescription": "SPI Bus Drivers"
        }
    ],
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "cfgElements": [
                        {
                            "contents": [
                                {
                                    "contents": [
                                        {
                                            "cfgName": "IO Serial Avail",
                                            "defaultValue": "",
                                            "defineName": "RTOS_MODULE_IO_SERIAL_AVAIL",
                                            "type": "configuration"
                                        },
                                        {
                                            "cfgName": "IO SPI Avail",
                                            "defaultValue": "",
                                            "defineName": "RTOS_MODULE_IO_SERIAL_SPI_AVAIL",
                                            "type": "configuration"
                                        }
                                    ],
                                    "title": "RTOS MODULES DESCRIPTION",
                                    "type": "category"
                                }
                            ],
                            "internalName": "rtos_description.h"
                        }
                    ],
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "rtos/io/include/spi.h",
                                "rtos/io/include/spi_slave.h",
                                "rtos/io/include/serial.h",
                                "rtos/io/source/serial/spi/*",
                                "rtos/io/source/serial/serial.c",
                                "rtos/io/source/serial/serial_ctrlr_priv.h",
                                "rtos/io/source/serial/serial_priv.h"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "MOS-SHRD-000000-P-P1",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.io.serial.drivers.*",
                            "multiplicity": "1..*"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.io.serial.spi",
            "inheritParentVersion": true,
            "longDescription": "SPI Module. Supports only master mode.",
            "multiplicity": "1",
            "name": "SPI (master)",
            "order": 30,
            "shortDescription": "SPI Master Module"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "rtos/drivers/io/include/serial_drv.h",
                                "rtos/drivers/io/source/serial_ctrlr_drv_rx_rspi.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.renesas.serial.rspi.rx"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "inherit",
                    "softwareComponentsRequirements": [
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.bsp.*.io.serial.renesas_rspi.*"
                                },
                                {
                                    "id": "com.micrium.rtos.bsp.template.io.spi"
                                }
                            ]
                        },
                        {
                            "id": "com.micrium.rtos.io.serial.spi"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.io.serial.drivers.renesas_rspi",
            "inheritParentVersion": true,
            "longDescription": "RX RSPI Driver",
            "multiplicity": "1",
            "name": "RX RSPI",
            "order": 50,
            "shortDescription": "Renesas RX RSPI Driver"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "rtos/drivers/io/include/serial_drv.h",
                                "rtos/drivers/io/source/serial_ctrlr_drv_kinetis_spi.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.nxp.serial.kinetis_spi"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "inherit",
                    "softwareComponentsRequirements": [
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.bsp.*.io.serial.kinetis_spi.*"
                                },
                                {
                                    "id": "com.micrium.rtos.bsp.template.io.spi"
                                }
                            ]
                        },
                        {
                            "id": "com.micrium.rtos.io.serial.spi"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.io.serial.drivers.kinetis_spi",
            "inheritParentVersion": true,
            "longDescription": "NXP Kinetis SPI Driver",
            "multiplicity": "1",
            "name": "Kinetis SPI Driver",
            "shortDescription": "NXP Kinetis SPI Driver"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "rtos/drivers/io/include/serial_drv.h",
                                "rtos/drivers/io/source/serial_ctrlr_drv_siliconlabs_gecko_sdk.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.serial.usart"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "inherit",
                    "softwareComponentsRequirements": [
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.rtos.bsp.*.io.serial.siliconlabs_gecko_sdk.*"
                                },
                                {
                                    "id": "com.micrium.rtos.bsp.template.io.spi"
                                }
                            ]
                        },
                        {
                            "id": "com.3party.vendor-libraries.siliconlabs.geckosdk"
                        },
                        {
                            "id": "com.micrium.rtos.io.serial.spi"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.io.serial.drivers.siliconlabs_gecko_sdk",
            "inheritParentVersion": true,
            "longDescription": "Driver for Silicon Labs USART module. Built on top of the spidrv module from the Gecko SDK.",
            "multiplicity": "1",
            "name": "Silicon Labs USART",
            "shortDescription": "Driver for Silicon Labs USART module"
        }
    ],
    "defaultVersion": "5.00.00",
    "inheritParentVersion": true,
    "version": "1.0"
}