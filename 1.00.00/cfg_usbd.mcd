{
    "cfgFiles": [
        {
            "contents": [
                {
                    "contents": [
                        {
                            "text": "USB Device product can be configured to be optimized for speed and determinism or for reduced RAM usage.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Optimize USB-Host for speed",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_CFG_OPTIMIZE_SPD",
                            "level": "basic",
                            "shortDesc": "Enable/disable optimization for speed.",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "OPTIMIZATION CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "If none of the USB device controller is used at High-Speed, USBD_CFG_HS_EN can be set to DEF_DISABLED.",
                            "type": "note"
                        },
                        {
                            "cfgName": "High-Speed support",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_CFG_HS_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for high-speed device.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "text": "Microsoft OS descriptors are useful for vendor class as it automates the process of loading the WinUSB driver without having to provide a .inf file.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Support for Microsoft OS descriptors",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_CFG_MS_OS_DESC_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for Microsoft OS descriptors.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Support of USB strings",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_CFG_STR_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support support of USB strings.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "text": "Isochronous endpoints must be enabled when using the audio class. Make sure your driver supports isochronous endpoints.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Support for Isochronous endpoints",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_CFG_EP_ISOC_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for Isochronous endpoints.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "text": "USB device supports submission of multiple URB when USBD_CFG_URB_EXTRA_EN is set to DEF_ENABLED. Make sure your driver supports URB queuing before setting this to DEF_ENABLED.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Support for multiple URB submission",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_CFG_URB_EXTRA_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for multiple URB submission.",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "GENERAL USB CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "cfgName": "Support for playback (speaker) streams",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_AUDIO_CFG_PLAYBACK_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for playback (speaker) streams.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Support for record (microphone) streams",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_AUDIO_CFG_RECORD_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for record (microphone) streams.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "text": "When USBD_AUDIO_CFG_FU_MAX_CTRL_EN is set to DEF_DISABLED, only 'mute' and 'volume' units are kept.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Support for extra feature units",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_AUDIO_CFG_FU_MAX_CTRL_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable support for extra feature units.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Support for mixer units",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_AUDIO_CFG_MU_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for mixer units.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Support for selector units",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_AUDIO_CFG_SU_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for selector units.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "text": "Audio class must compensate for jitter and clock differences between host and device. This can be done by either enabling the feedback endpoint for playback interfaces (USBD_AUDIO_CFG_PLAYBACK_FEEDBACK_EN) or by enabling software correction (USBD_AUDIO_CFG_PLAYBACK_CORR_EN / USBD_AUDIO_CFG_RECORD_CORR_EN).",
                            "type": "note"
                        },
                        {
                            "cfgName": "Feedback endpoint for playback interfaces",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_AUDIO_CFG_PLAYBACK_FEEDBACK_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for feedback endpoint for playback interfaces.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Software correction for playback interfaces",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_AUDIO_CFG_PLAYBACK_CORR_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for software correction for playback interfaces.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Software correction for record interfaces",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_AUDIO_CFG_RECORD_CORR_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable support for software correction for record interfaces.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Debug statistics",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_AUDIO_CFG_STAT_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable debug statistics.",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "AUDIO CLASS CONFIGURATION",
                    "type": "category"
                },
                {
                    "contents": [
                        {
                            "text": "Host queries the device periodically to ensure media is still present. Since for some media type the 'StatusGet' operation can take some time (for example, SD cards), it is recommended to use the refresh task.",
                            "type": "note"
                        },
                        {
                            "cfgName": "Media state refresh task",
                            "defaultValue": "DEF_ENABLED",
                            "defineName": "USBD_SCSI_CFG_REFRESH_TASK_EN",
                            "level": "basic",
                            "shortDesc": "Enable/disable media state refresh task.",
                            "type": "configuration",
                            "values": "boolean"
                        },
                        {
                            "cfgName": "Support 64 bit Logical Block Address (LBA)",
                            "defaultValue": "DEF_DISABLED",
                            "defineName": "USBD_SCSI_CFG_64_BIT_LBA_EN",
                            "level": "advanced",
                            "shortDesc": "Enable/disable support for 64 bit Logical Block Address (LBA).",
                            "type": "configuration",
                            "values": "boolean"
                        }
                    ],
                    "title": "MSC SCSI CONFIGURATION",
                    "type": "category"
                }
            ],
            "fileName": "usbd_cfg.h",
            "includeFiles": [
                "<rtos/common/include/lib_def.h>"
            ],
            "title": "USB device Configuration"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}