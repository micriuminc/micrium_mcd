{
    "categories": [
        {
            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a",
            "name": "SLWSTK6000A",
            "order": 99,
            "shortDescription": "Board Support Package for SiliconLabs's SLWSTK6000A"
        },
        {
            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.toolchain.port",
            "name": "Toolchain ports",
            "order": 99,
            "shortDescription": "Ports for various toolchains"
        }
    ],
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "compilerDefines": {
                        "DEBUG_EFM": "",
                        "EFR32MG1P232F256GM48": ""
                    },
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/slwstk6000a/include/bsp_int.h",
                                "bsp/siliconlabs/slwstk6000a/include/bsp_led.h",
                                "bsp/siliconlabs/slwstk6000a/source/startup/system_efr32mg1p.c",
                                "bsp/siliconlabs/slwstk6000a/source/bsp.c",
                                "bsp/siliconlabs/slwstk6000a/source/bsp_cpu.c",
                                "bsp/siliconlabs/slwstk6000a/source/bsp_os.c",
                                "bsp/siliconlabs/slwstk6000a/source/bsp_led.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.board.slwstk6000a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.toolchain.port.*"
                        },
                        {
                            "id": "com.micrium.rtos.cpu"
                        },
                        {
                            "id": "com.micrium.rtos.common"
                        },
                        {
                            "id": "com.3party.vendor-libraries.siliconlabs.efr32mg1p"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel"
                                }
                            ]
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2.port.armv7-m.*"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel.port.armv7-m.*"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.core",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for Core components such as timers, uart, i2c, etc.",
            "multiplicity": "1",
            "name": "Core BSP Components",
            "order": 1,
            "shortDescription": "Board Support Package for Core components such as timers, uart, i2c, etc."
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/slwstk6000a/cfg/iar/EFR32MG1P232F256.icf",
                                "bsp/siliconlabs/slwstk6000a/source/startup/iar/startup_efr32mg1p.s"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.iar"
                        },
                        {
                            "id": "com.siliconlabs.board.slwstk6000a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.toolchain.port.iar",
            "inheritParentVersion": true,
            "longDescription": "IAR toolchain port.",
            "multiplicity": "1",
            "name": "IAR BSP Port",
            "order": 60,
            "shortDescription": "IAR toolchain port"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/slwstk6000a/cfg/gcc/efr32mg1p.ld",
                                "bsp/siliconlabs/slwstk6000a/source/startup/gcc/startup_efr32mg1p.S"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.gnu"
                        },
                        {
                            "id": "com.siliconlabs.board.slwstk6000a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.slwstk6000a.toolchain.port.gnu",
            "inheritParentVersion": true,
            "longDescription": "GNU toolchain port.",
            "multiplicity": "1",
            "name": "GNU BSP Port",
            "order": 60,
            "shortDescription": "GNU toolchain port"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}