{
    "categories": [
        {
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval",
            "name": "STM3240G-EVAL",
            "order": 99,
            "shortDescription": "Board Support Package for ST's STM3240G-Eval"
        },
        {
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.toolchain.port",
            "name": "Toolchain ports",
            "order": 99,
            "shortDescription": "Ports for various toolchains"
        }
    ],
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "compilerDefines": {
                        "NDEBUG": "",
                        "STM32F407xx": "",
                        "USE_HAL_DRIVER": ""
                    },
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/include/*",
                                "bsp/st/stm3240g_eval/README.txt",
                                "bsp/st/stm3240g_eval/README_ISR_HANDLER.txt",
                                "bsp/st/stm3240g_eval/source/startup/system_stm32f4xx.c",
                                "bsp/st/stm3240g_eval/source/bsp.c",
                                "bsp/st/stm3240g_eval/source/bsp_clk.c",
                                "bsp/st/stm3240g_eval/source/bsp_cpu.c",
                                "bsp/st/stm3240g_eval/source/bsp_led.c",
                                "bsp/st/stm3240g_eval/source/bsp_os.c",
                                "bsp/st/stm3240g_eval/cfg/stm32f4xx_hal_conf.h"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "includePaths": [
                        "Micrium/Micrium_OS_V5.00/bsp/st/stm3240g_eval/cfg"
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.toolchain.port.*"
                        },
                        {
                            "id": "com.micrium.rtos.cpu"
                        },
                        {
                            "id": "com.micrium.rtos.common"
                        },
                        {
                            "id": "com.3party.vendor-libraries.st.stm34cubef4"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel"
                                }
                            ]
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2.port.armv7-m.*"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel.port.armv7-m.*"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for Core components such as timers, uart, i2c, etc.",
            "multiplicity": "1",
            "name": "Core BSP Components",
            "order": 1,
            "shortDescription": "Board Support Package for Core components such as timers, uart, i2c, etc."
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/bsp_usb_dev.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        },
                        {
                            "id": "com.micrium.rtos.usb.device.driver.dwc_otg_fs"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.usbd.dwc_otg_fs.otg-fs",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the USB port on the STM3240G-Eval.",
            "multiplicity": "1",
            "name": "USB-Device USB_FS Controller",
            "order": 20,
            "shortDescription": "Board Support Package for the USB port on the STM3240G-Eval"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/bsp_usb_host_pbhcd.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        },
                        {
                            "id": "com.micrium.rtos.usb.host.driver.dwc_otg_fs"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.usbh.dwc_otg_fs.otg-fs",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the USB Host port on the STM3240G-Eval.",
            "multiplicity": "1",
            "name": "USB-Host USB_FS Controller",
            "order": 20,
            "shortDescription": "Board Support Package for the USB Host port on the STM3240G-Eval"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/bsp_fs_sd_card.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        },
                        {
                            "id": "com.micrium.rtos.fs.driver.sd.card"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.fs_sd",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the SD card controller on the STM3240G-Eval.",
            "multiplicity": "1",
            "name": "FS SD Card Controller",
            "order": 30,
            "shortDescription": "Board Support Package for the SD card controller on the STM3240G-Eval"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/bsp_net_ether_gmac.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        },
                        {
                            "id": "com.micrium.rtos.net.drivers.ether.gmac"
                        },
                        {
                            "id": "com.micrium.rtos.net.drivers.ether.phy.generic"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.net_ether.gmac",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the GMAC Ethernet controller on the STM3240G-Eval board.",
            "multiplicity": "1",
            "name": "Network Ethernet GMAC Controller",
            "order": 40,
            "shortDescription": "Board Support Package for the GMAC Ethernet controller on the STM3240G-Eval"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/cfg/iar/stm32f407xx_flash.icf",
                                "bsp/st/stm3240g_eval/source/startup/iar/startup_stm32f407xx.s"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.iar"
                        },
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.toolchain.port.iar",
            "inheritParentVersion": true,
            "longDescription": "EWARM toolchain port.",
            "multiplicity": "1",
            "name": "IAR BSP Port",
            "order": 70,
            "shortDescription": "EWARM toolchain port"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/startup/gnu/startup_stm32f407xx.s"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.gnu"
                        },
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.toolchain.port.gnu",
            "inheritParentVersion": true,
            "longDescription": "GNU toolchain port.",
            "multiplicity": "1",
            "name": "GNU BSP Port",
            "order": 50,
            "shortDescription": "GNU toolchain port"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/st/stm3240g_eval/source/startup/armcc/system_stm32f407xx.s"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.armcc"
                        },
                        {
                            "id": "com.st.board.stm3240g_eval"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.st.stm3240g_eval.toolchain.port.armcc",
            "inheritParentVersion": true,
            "longDescription": "ARMCC toolchain port.",
            "multiplicity": "1",
            "name": "ARMCC BSP Port",
            "order": 50,
            "shortDescription": "ARMCC toolchain port"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}