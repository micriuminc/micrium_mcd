{
    "categories": [
        {
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a",
            "name": "EFM32GG-STK3700a",
            "order": 99,
            "shortDescription": "Board Support Package for SiliconLabs's EFM32GG-STK3700a"
        },
        {
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.toolchain.port",
            "name": "Toolchain ports",
            "order": 99,
            "shortDescription": "Ports for various toolchains"
        }
    ],
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "compilerDefines": {
                        "DEBUG_EFM": "",
                        "EFM32GG990F1024": ""
                    },
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/source/startup/system_efm32gg.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_clk.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_cpu.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_led.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_os.c",
                                "bsp/siliconlabs/efm32gg_stk3700a/include/bsp_clk.h",
                                "bsp/siliconlabs/efm32gg_stk3700a/include/bsp_int.h",
                                "bsp/siliconlabs/efm32gg_stk3700a/include/bsp_led.h"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.toolchain.port.*"
                        },
                        {
                            "id": "com.micrium.rtos.cpu"
                        },
                        {
                            "id": "com.micrium.rtos.common"
                        },
                        {
                            "id": "com.3party.vendor-libraries.siliconlabs.efm32gg"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel"
                                }
                            ]
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2.port.armv7-m.*"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel.port.armv7-m.*"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for Core components such as timers, uart, i2c, etc.",
            "multiplicity": "1",
            "name": "Core BSP Components",
            "order": 1,
            "shortDescription": "Board Support Package for Core components such as timers, uart, i2c, etc."
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_fs_nand_ctrlr_gen.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core"
                        },
                        {
                            "id": "com.micrium.rtos.fs.driver.nand"
                        },
                        {
                            "id": "com.micrium.rtos.fs.driver.nand.ctrl.soft_ecc"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.fs_nand",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the NAND controller on the EFM32GG-STK3700a.",
            "multiplicity": "1",
            "name": "FS NAND Controller",
            "order": 50,
            "shortDescription": "Board Support Package for the NAND controller on the EFM32GG-STK3700a"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_usb_dev.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core"
                        },
                        {
                            "id": "com.micrium.rtos.usb.device.driver.dwc_otg_fs"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.usbd.dwc_otg_fs.usb",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the USB Device port on the EFM32GG-STK3700a.",
            "multiplicity": "1",
            "name": "USB-Device Controller",
            "order": 50,
            "shortDescription": "Board Support Package for the USB Device port on the EFM32GG-STK3700a"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/source/bsp_usb_host_pbhcd.c"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core"
                        },
                        {
                            "id": "com.micrium.rtos.usb.host.driver.dwc_otg_fs"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.usbh.dwc_otg_fs.usb",
            "inheritParentVersion": true,
            "longDescription": "Board Support Package for the USB Host port on the EFM32GG-STK3700a.",
            "multiplicity": "1",
            "name": "USB-Host Controller",
            "order": 50,
            "shortDescription": "Board Support Package for the USB Host port on the EFM32GG-STK3700a"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/cfg/iar/EFM32GG990F1024.icf",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/startup/iar/startup_efm32gg.s"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.iar"
                        },
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.toolchain.port.iar",
            "inheritParentVersion": true,
            "longDescription": "IAR toolchain port",
            "multiplicity": "1",
            "name": "IAR BSP Port",
            "order": 50,
            "shortDescription": "IAR toolchain port"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/siliconlabs/efm32gg_stk3700a/cfg/gcc/efm32gg.ld",
                                "bsp/siliconlabs/efm32gg_stk3700a/source/startup/gcc/startup_efm32gg.S"
                            ]
                        }
                    ],
                    "hardwareRequirements": [
                        {
                            "id": "com.toolchain.gnu"
                        },
                        {
                            "id": "com.siliconlabs.board.efm32gg_stk3700a"
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.core"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.siliconlabs.efm32gg_stk3700a.toolchain.port.gnu",
            "inheritParentVersion": true,
            "longDescription": "GNU toolchain port.",
            "multiplicity": "1",
            "name": "GNU BSP Port",
            "order": 60,
            "shortDescription": "GNU toolchain port"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}