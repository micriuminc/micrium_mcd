{
    "categories": [
        {
            "id": "com.micrium.rtos.bsp.template",
            "name": "Template",
            "order": 99,
            "shortDescription": "Template for Board Support Packages"
        }
    ],
    "components": [
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp.c",
                                "bsp/template/source/bsp_cpu.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.cpu",
                            "multiplicity": "1"
                        },
                        {
                            "id": "com.micrium.rtos.common",
                            "multiplicity": "1"
                        },
                        {
                            "oneOf": [
                                {
                                    "id": "com.micrium.legacy.os2",
                                    "multiplicity": "1"
                                },
                                {
                                    "id": "com.micrium.rtos.kernel",
                                    "multiplicity": "1"
                                }
                            ]
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.general.core",
            "inheritParentVersion": true,
            "longDescription": "Use this Board Support Package if no BSP is available for your board.",
            "multiplicity": "1",
            "name": "Template Core BSP",
            "order": 1,
            "shortDescription": "Template to use as a starting point for core BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_usb_dev.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.usb.device",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.usb.device",
            "inheritParentVersion": true,
            "longDescription": "Template for USB Device module BSP.",
            "multiplicity": "1",
            "name": "Template for USB-Device Controller",
            "order": 50,
            "shortDescription": "Template for USB Device module BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_usb_host_hcd.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.usb.host",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.usb.host.hcd",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if you use a standard USB Host HCD (OHCI or EHCI).",
            "multiplicity": "1",
            "name": "Template for USB-Host HCD Controller",
            "order": 50,
            "shortDescription": "Template for USB Host module HCD BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_usb_host_pbhcd.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.usb.host",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.usb.host.pbhcd",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if you use a Pipe-Based USB Host Controller Driver (anything other than OHCI or EHCI).",
            "multiplicity": "1",
            "name": "Template for USB-Host PBHCD Controller",
            "order": 50,
            "shortDescription": "Template for USB Host module PBHCD BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_net_ether.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.net.drivers.ether"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.net.ether",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if you will be creating a new driver.",
            "multiplicity": "1",
            "name": "Template for Network Ethernet Controller",
            "order": 40,
            "shortDescription": "Template for Network Ethernet Controller BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_net_wifi.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.net.drivers.wifi.*",
                            "multiplicity": "1..*"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.net.wifi",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if 1) you are creating a new driver or 2) you are using a Wi-Fi device with an existing driver on a board that does not have a BSP for it (including a custom board).",
            "multiplicity": "1",
            "name": "Template for Network WiFi Device",
            "order": 41,
            "shortDescription": "Template for Network WiFi Controller BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_spi.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.io.serial.drivers.*",
                            "multiplicity": "1..*"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.io.spi",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if you are using an existing driver on a board that does not have a BSP for it (including a custom board) for the SPI controller.",
            "multiplicity": "1",
            "name": "Template for SPI",
            "order": 50,
            "shortDescription": "Template for SPI BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_fs_nand_ctrlr_gen.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.fs",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.fs.nand",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if 1) you are creating a new driver or 2) you are using an existing driver on a board that does not have a BSP for it (including a custom board).",
            "multiplicity": "1",
            "name": "Template for FS NAND",
            "order": 50,
            "shortDescription": "Template for File System NAND BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_fs_nor_quad_spi.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.fs",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.fs.nor.quad_spi",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if 1) you are creating a new driver or 2) you are using an existing driver on a board that does not have a BSP for it (including a custom board).",
            "multiplicity": "1",
            "name": "Template for FS NOR QSPI",
            "order": 50,
            "shortDescription": "Template for File System NOR QSPI BSP"
        },
        {
            "details": [
                {
                    "branch": "release/5.00.00",
                    "files": [
                        {
                            "exportDestParent": "Micrium/Micrium_OS_V5.00",
                            "filenames": [
                                "bsp/template/source/bsp_fs_sd_card.c"
                            ]
                        }
                    ],
                    "locationType": "Git",
                    "repoUrl": "git@bitbucket.org:micriuminc/micrium_rtos.git",
                    "sku": "none",
                    "softwareComponentsRequirements": [
                        {
                            "id": "com.micrium.rtos.fs",
                            "multiplicity": "1"
                        }
                    ]
                }
            ],
            "documentationUrl": "",
            "id": "com.micrium.rtos.bsp.template.fs.sd.card",
            "inheritParentVersion": true,
            "longDescription": "Use this template BSP if 1) you are creating a new driver or 2) you are using an existing driver on a board that does not have a BSP for it (including a custom board).",
            "multiplicity": "1",
            "name": "Template for FS SD Card",
            "order": 50,
            "shortDescription": "Template for File System SD Card BSP"
        }
    ],
    "defaultVersion": "5.00.00",
    "version": "1.0"
}